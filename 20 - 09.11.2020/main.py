import requests
from flask import Flask, render_template

from env import OPENWEATHER_API_KEY

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    query = "Ljubljana"
    unit = "metric"

    url = "https://api.openweathermap.org/data/2.5/weather?q={0}&units={1}&appid={2}".format( query, unit, OPENWEATHER_API_KEY )

    data = requests.get(url=url)

    return render_template("index.html", data=data.json())

if __name__ == '__main__':
    app.run()


# api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
# api.openweathermap.org/data/2.5/weather?q=Ljubljana&appid=4d141b52b616e644babf3acb2a70ff5e
# ?q=content&param2=content2&param3=content3
# GET REQUESTS

# CACHE - CACHING STORE DATA FOR A WHILE AND WHEN NOT VALID ANYMORE, GET THAT DATA AGAIN
# url -> api.com/weather?city=LJ&unit=metric&apikey=i124902384230948
# -> 09.11.2020 at 18:00 -> api.com/weather?city=LJ&unit=metric&apikey=i124902384230948 -> temperature: "5"
#