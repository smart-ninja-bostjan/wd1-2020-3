from pyowm import OWM

from env import OPENWEATHER_API_KEY

owm = OWM(OPENWEATHER_API_KEY)
mgr = owm.weather_manager()

observation = mgr.weather_at_place('Ljubljana')
w = observation.weather

print(w)

temperature = w.temperature('celsius')
print(temperature)