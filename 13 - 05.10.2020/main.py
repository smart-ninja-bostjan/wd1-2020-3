import random
from datetime import datetime
from prettytable import PrettyTable

random_num = random.randint( 1, 50 )
print( random_num )

time_now = datetime.now()
print( time_now )

table = PrettyTable( [ "animal", "ferocity" ] )

table.add_row( [ "wolverine", 100 ] )
table.add_row( [ "grizzly", 81 ] )
table.add_row( [ "cat", -1 ] )

print( table )