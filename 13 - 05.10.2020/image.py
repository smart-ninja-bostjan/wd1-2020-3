# https://tannerhelland.com/2011/10/01/grayscale-image-algorithm-vb6.html

from PIL import Image

im = Image.open( 'avatar.jpg', 'r' )

# [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ... ]
# Size: 3

# Photoshop
def convert_to_black_and_white( im, algorithm = "Green" ):
    bw_image = []
    for color in im.getdata():
        if algorithm == "Green":
            green_value = color[ 1 ]
            bw_value = ( green_value, green_value, green_value )
        elif algorithm == "Photoshop":
            value = int( color[ 0 ] * 0.3 + color[ 1 ] * 0.59 + color[ 2 ] * 0.11 )
            bw_value = ( value, value, value )
        bw_image.append( bw_value )
    bw = Image.new( im.mode, im.size )
    bw.putdata( bw_image )
    return bw

bw = convert_to_black_and_white( im )
bw.save( 'avatar_bw_green.jpg' )

bw = convert_to_black_and_white( im, "Photoshop" )
bw.save( 'avatar_bw_photoshop.jpg' )

# R, G, B -> 0 - 255
# ( 215, 218, 141 ) ( 210, 212, 165 ) ( 149, 150, 145 )
# ( 215, 218, 141 ) ( 210, 212, 165 ) ( 149, 150, 145 )
# ( 215, 218, 141 ) ( 210, 212, 165 ) ( 149, 150, 145 )