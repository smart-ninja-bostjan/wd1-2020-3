# R - red player
# B - blue player

# R R R
# O O O
# B B B

# Empty battlefield
# False False False
# False False False
# False False False
#
#   0  1  2
# [ R, R, R ] 0   -> ( 0, 0 ) -> R
# [ G, G, G ] 1   -> ( 0, 2 ) -> B
# [ B, B, B ] 2

def generate_playfield( size = 6 ):
    battlefield = []
    for i in range( 0, size ):
        row = [ False ] * size # Without using for loop
        battlefield.append( row )
    return battlefield

def game_over( playfield, player ):
    if player == 'R':
        seek = 'B'
    else:
        seek = 'R'
    for row in playfield:
        for column in row:
            if column == seek:
                return False
    return True

def hit( playfield, x, y, player ):
    if playfield[ x ][ y ]:
        value = playfield[ x ][ y ]
        if value == player:
            return False
        playfield[ x ][ y ] = False
        return True
    return False

size = int( input( "Enter field size: " ) )
playfield = generate_playfield( size )

test_playfield = [
    [ 'R', False, 'R' ],
    [ 'B', 'B', False ],
    [ False, False, False ]
]

hit = hit( test_playfield, 0, 0, 'B' )
if hit:
    print( "You sunk a battleship." )
    game_over( test_playfield, 'B' )
    print( test_playfield )

# Ask user where to put ships!
"""
while True:
    position = input( "Where shall I put your ship? (x, y)" )
    positions = position.split( ',' )

    x = int( positions[ 0 ] ) - 1
    y = int( positions[ 1 ] ) - 1 # So user does not enter 0

    playfield[ x ][ y ] = 'R'

    print( playfield )
"""