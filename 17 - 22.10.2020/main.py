from flask import Flask, render_template, request, redirect, url_for
from models import User, Message, db

app = Flask( __name__ )
db.create_all()

# REQUESTS:
#   - GET
#   - POST
#   - DELETE
#   - PUT
#   - PATCH

@app.route( "/" )
def index():
    users = db.query( User ).all()
    return render_template( "index.html", users=users )

@app.route( "/user", methods=[ "GET", "POST" ] )
def user():
    if request.method == "POST":
        name = request.form.get( "name" )
        email = request.form.get( "email" )

        user_db = User( name=name, email=email )

        db.add( user_db )
        db.commit()

        return redirect( url_for( 'index' ) )

    elif request.method == "GET":
        return render_template( "user.html" )
    else:
        return "Unsupported method."

@app.route( "/message", methods=[ "GET", "POST" ] )
def message():
    if request.method == "POST":
        text = request.form.get( "text" )
        user = int( request.form.get( "user" ) )

        message_db = Message( text=text, user_id=user )

        db.add( message_db )
        db.commit()

        return redirect( url_for( 'index' ) )

    elif request.method == "GET":
        users = db.query( User ).all()
        return render_template( "message.html", users=users )
    else:
        return "Unsupported method."


if __name__ == '__main__':
    app.run()