import os
from sqla_wrapper import SQLAlchemy

db = SQLAlchemy( os.getenv( "DATABASE_URL", "sqlite:///localhost.sqlite" ) )

class User( db.Model ):
    __tablename__ = 'users'
    id = db.Column( db.Integer, primary_key= True )
    name = db.Column( db.String )
    email = db.Column( db.String, unique = True )
    messages = db.relationship( "Message" )

class Message( db.Model ):
    __tablename__ = 'messages'
    id = db.Column( db.Integer, primary_key = True )
    text = db.Column( db.String )
    user_id = db.Column( db.Integer, db.ForeignKey( 'users.id' ) )

# users ->
#   id (primary key, 1 -> ... )
#   name (string)
#   email (string, unique)
#
# messages ->
#   id (primary key, 1 -> ... )
#   text (string)
#   user_id (foreign key -> on users, id)

# users INSERT:
#    id -> 1
#    name -> bostjan
#    email -> bostjan.cigan@foo.com
#
# messages INSERT:
#    id -> 1
#    text -> "Buy milk"
#    user_id -> 1