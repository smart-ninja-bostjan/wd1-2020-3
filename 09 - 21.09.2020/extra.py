# Append into file example, does not override content
with open( "content.txt", "a" ) as content_file:
    content_file.write( "Fooo!" )

# CSV? -> Comma Separated Value
"""
Ime,Priimek,Telefon,Spol
Floogy,McFloo,12345,M
Floofy,McFloo,231231,M
Blabla,Bloblo,212313,F
Urša,Bizjak,12123,F
"""
# Each line is separated by a new line (\n)
# Each column data in a row is separated by a separator - delimiter (default -> , or |, ; ...)

# XML
"""
<?xml version="1.0" encoding="UTF-8"?>
<people>

  <person>
    <name>Tina</name>
    <age>23</age>
    <gender>female</gender>
  </person>

  <person>
    <name>Jakob</name>
    <age>35</age>
    <gender>male</gender>
  </person>

  <person>
    <name>Barbara</name>
    <age>44</age>
    <gender>female</gender>
  </person>

</people>

--> Name    Age     Gender
    Tina    23      female
    Jakob   35      male
    Barbara 44      female
    
"""
# JSON
"""
{
    "people":[
        {
            "name": "Tina",
            "age": 23,
            "gender": "female"
        },

        {
            "name": "Jakob",
            "age": 35,
            "gender": "male"
        },

        {
            "name": "Barbara",
            "age": 44,
            "gender": "female"
        }
    ]
}
"""

# YAML
"""
people:
- name: Tina
  age: '23'
  gender: female

- name: Jakob
  age: '35'
  gender: male

- name: Barbara
  age: '44'
  gender: female
"""