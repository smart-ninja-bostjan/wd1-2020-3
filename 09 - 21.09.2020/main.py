import random

# "r"  -> READ
# "w"  -> WRITE
# "rw" -> READ, WRITE
# "a"  -> APPEND  (add to file and not override content)

"""
with open( "content.txt", "r" ) as shopping_list_file:
    content = shopping_list_file.read().splitlines()
    for item in content:
        print( item )

with open( "content.txt", "w" ) as shopping_list_file:
    shopping_list_file.write( "This is something I added." )
"""

# 1. Generate secret number
# 2. Attempts (attempts) stores the number of attempts the user has tried to guess
# 3. Open file score.txt and read contents (most successfull number of attempts)
# 4. Code from previous lesson (guess the secret number)
# 5. Add write into file if number of attempts is smaller

# Step 1
secret = random.randint( 1, 30 )
# Step 2
attempts = 0

# Step 3:
with open( "score.txt", "r" ) as score_file:
    best_score = int( score_file.read() )
    print( "Best score: " + str( best_score ) )

# Step 4:
while True:
    guess = int( input( "Enter your guess please: " ) )
    # Step 2
    attempts = attempts + 1 # This is the same as writing attempts += 1

    if guess == secret:
        print( "You have guessed it!" )
        print( "Attempts needed: " + str( attempts ) )

        if attempts < best_score: # Max 1x
            with open( "score.txt", "w" ) as score_file:
                score_file.write( str( attempts ) )
        break
    elif guess < secret:
        print( "Your guess is not correct. Try something bigger." )
    else: # elif guess > secret
        print( "Your guess is not correct. Try smaller." )


