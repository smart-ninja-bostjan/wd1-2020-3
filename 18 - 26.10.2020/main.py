import hashlib
import random
import uuid

from flask import Flask, render_template, request, make_response, redirect, url_for
from models import User, db

#HASH_SALT = "LRmzAnfOkcezcbSA1OJ7B2RE6d3gJ9j2MTHhKASsl1i6KdMiQByyjeoK9q1Uguidvb7aIb4GEb3TQ71aI0SSfBmcl7shXpkd5ZZW9XSC6TgrzvnNEYAdbDuP"

app = Flask(__name__)
db.create_all()  # create (new) tables in the database

# HASHING??
# HASH IS A MATHEMATICAL FUNCTION
# IT GOES ONLY ONE WAY!
# f(x) = a -> b
#        b -> c
#        c -> d
#        ...
# abc -> bcd
#
# LOGIN:
#   1.) password -> f( password ) ... -> aksfoakdaopfkaopfkdso
#   2.) logout
#   3.) login -> f( password ) ... -> aksfoakdaopfkaopfkdso

# SALTING?
#   1) geslo
#   2) SALT=skfpoekfpoejfpoejfpoejfpoejfwpeokfpwoekfpwoefkwpoefkpwoekf
#   3) f( geslo ) -> f ( SALT + geslo )

@app.route("/", methods=["GET"])
def index():
    session_token = request.cookies.get('session_token')

    if session_token:
        user = db.query(User).filter_by(session_token=session_token).first()
    else:
        user = None

    return render_template("index.html", user=user)


@app.route("/login", methods=["POST"])
def login():
    name = request.form.get("user-name")
    email = request.form.get("user-email")
    password = request.form.get("user-password")

    hashed_password = hashlib.sha256(password.encode()).hexdigest()
    #hashed_password = hashlib.sha256((HASH_SALT+password).encode()).hexdigest()

    # create a secret number
    secret_number = random.randint(1, 30)

    # see if user already exists
    user = db.query(User).filter_by(email=email).first()

    if not user:
        # create a User object
        user = User(name=name, email=email, secret_number=secret_number, password=hashed_password)

        # save the user object into a database
        db.add(user)
        db.commit()

    if hashed_password != user.password:
        return "WRONG PASSWORD! Go back and try again!"
    elif hashed_password == user.password:
        session_token = str(uuid.uuid4())
        user.session_token = session_token

        db.add(user)
        db.commit()

        response = make_response(redirect(url_for('index')))
        response.set_cookie('session_token', session_token, httponly=True, samesite='Strict')
        # secure=True

        return response


@app.route("/result", methods=["POST"])
def result():
    guess = int(request.form.get("guess"))

    session_token = request.cookies.get("session_token")

    # get user from the database based on her/his email address
    user = db.query(User).filter_by(session_token=session_token).first()

    if guess == user.secret_number:
        message = "Correct! The secret number is {0}".format(str(guess))

        # create a new random secret number
        new_secret = random.randint(1, 30)

        # update the user's secret number
        user.secret_number = new_secret

        # update the user object in a database
        db.add(user)
        db.commit()
    elif guess > user.secret_number:
        message = "Your guess is not correct... try something smaller."
    elif guess < user.secret_number:
        message = "Your guess is not correct... try something bigger."

    return render_template("result.html", message=message)


if __name__ == '__main__':
    app.run()  # if you use the port parameter, delete it before deploying to Heroku
