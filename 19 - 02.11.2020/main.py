import hashlib
import random
import uuid

from flask import Flask, g, render_template, request, make_response, redirect, url_for
from functools import wraps
from models import User, db

app = Flask(__name__)
db.create_all()  # create (new) tables in the database

# This is a login required decorator that we can add to our handlers
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs): # Here we do the same logic for fetching our users
        session_token = request.cookies.get("session_token")
        # If session does not exists, we simply redirect to index
        if not session_token:
            return redirect(url_for("index"))

        # We get the user from DB
        user = db.query(User).filter_by(session_token=session_token).first()

        # If he does not exist, we simply redirect to index
        if not user:
            return redirect(url_for("index"))

        # Now we return the wrapped call (so we call our handler with the parameter user and other arguments)
        return f(user, *args, **kwargs)
    # We return the wrapped function
    return wrap

#@app.before_request
def middleware_user():
    # DOWNFALL OF THIS APPROACH:
    # 1) If we do not need it then it will get triggered and also push
    #    a request to the DB
    # 2) It will trigger at each request, no exceptions
    # More useful for logging etc.
    # https://pythonise.com/series/learning-flask/python-before-after-request
    session_token = request.cookies.get("session_token")
    if session_token:
        user = db.query(User).filter_by(session_token=session_token).first()
    else:
        user = None
    g.user = user

# Testing our login_required decorator
@app.route("/test", methods=["GET"])
@login_required
def test(user):
    # The user holds our DB user so that we don't have to duplicate our logic
    # anymore, open browser in incognito mode and you will see you will get redirected
    # to main page and this code will not be triggered
    print(user.name)
    return render_template("test.html")

@app.route("/", methods=["GET"])
def index():
    session_token = request.cookies.get("session_token")

    if session_token:
        user = db.query(User).filter_by(session_token=session_token).first()
    else:
        user = None

    return render_template("index.html", user=user)


@app.route("/login", methods=["POST"])
def login():
    name = request.form.get("user-name")
    email = request.form.get("user-email")
    password = request.form.get("user-password")

    # hash the password
    hashed_password = hashlib.sha256(password.encode()).hexdigest()

    # create a secret number
    secret_number = random.randint(1, 30)

    # see if user already exists
    user = db.query(User).filter_by(email=email).first()

    if not user:
        # create a User object
        user = User(name=name, email=email, secret_number=secret_number, password=hashed_password)

        # save the user object into a database
        db.add(user)
        db.commit()

    # check if password is incorrect
    if hashed_password != user.password:
        return "WRONG PASSWORD! Go back and try again."
    elif hashed_password == user.password:
        # create a random session token for this user
        session_token = str(uuid.uuid4())

        # save the session token in a database
        user.session_token = session_token
        db.add(user)
        db.commit()

        # save user's session token into a cookie
        response = make_response(redirect(url_for('index')))
        response.set_cookie("session_token", session_token)  #  consider adding httponly=True on production

        return response


@app.route("/result", methods=["POST"])
def result():
    guess = int(request.form.get("guess"))

    session_token = request.cookies.get("session_token")

    # get user from the database based on her/his email address
    user = db.query(User).filter_by(session_token=session_token).first()

    if guess == user.secret_number:
        message = "Correct! The secret number is {0}".format(str(guess))

        # create a new random secret number
        new_secret = random.randint(1, 30)

        # update the user's secret number
        user.secret_number = new_secret

        # update the user object in a database
        db.add(user)
        db.commit()
    elif guess > user.secret_number:
        message = "Your guess is not correct... try something smaller."
    elif guess < user.secret_number:
        message = "Your guess is not correct... try something bigger."

    return render_template("result.html", message=message)

@app.route("/profile", methods=["GET"])
def profile():
    session_token = request.cookies.get("session_token")

    user = db.query(User).filter_by(session_token=session_token).first()

    if user:
        return render_template("profile.html", user=user)
    else:
        return redirect(url_for("index"))

@app.route("/profile/edit", methods=["GET","POST"])
def profile_edit():
    session_token = request.cookies.get("session_token")

    user = db.query(User).filter_by(session_token=session_token).first()

    if request.method == "GET":
        if user:
            return render_template("profile_edit.html", user=user)
        else:
            return redirect(url_for("index"))
    elif request.method == "POST":
        name = request.form.get("profile-name")
        email = request.form.get("profile-email")

        user.name = name
        user.email = email

        db.add(user)
        db.commit()

        return redirect(url_for("profile"))

@app.route("/profile/delete", methods=["GET","POST"])
def profile_delete():
    session_token = request.cookies.get("session_token")

    user = db.query(User).filter_by(session_token=session_token).first()

    if request.method == "GET":
        if user:
            return render_template("profile_delete.html", user=user)
        else:
            return redirect(url_for("index"))
    elif request.method == "POST":
        db.delete(user)
        db.commit()

        return redirect(url_for("index"))

@app.route("/users", methods=["GET"])
def all_users():
    users = db.query(User).all()

    return render_template("users.html", users=users)

@app.route("/user/<user_id>", methods=["GET"])
def user_details(user_id):
    user = db.query(User).get(int(user_id))

    return render_template("user_details.html",user=user)


if __name__ == '__main__':
    app.run()
