class Cart():
    def __init__(self, user):
        self.user = user
        self.items = []

    def add_to_cart(self, price, item):
        self.items.append({
            "price": price,
            "item": item
        })

    def get_items(self):
        return self.items

    def calculate_price(self):
        price = 0
        for item in self.items:
            price = price + item["price"]

        return price