import random
from faker import Faker
fake = Faker()

# first, import a similar Provider or use the default one
from faker.providers import BaseProvider

items = [
    'banana',
    'orange',
    'pear',
    'plum',
    'tires',
    'commodore64',
    'boots'
]

# create new provider class
class CartProvider(BaseProvider):
    def cart_item(self):
        return random.choice(items)

# then add new provider to faker instance
fake.add_provider(CartProvider)

# now you can use:
item = fake.cart_item()
item2 = fake.cart_item()
print(item)
print(item2)