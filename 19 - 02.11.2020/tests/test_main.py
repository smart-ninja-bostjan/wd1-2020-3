import os
import pytest
from faker import Faker
from main import app, db

faker = Faker()

# TDD -> Test Driven Development
#
# - Unit Testing
# - Integration Testing
# - UX / UI Testing
#
# CI / CD -> Continuous Integration, Continuous Delivery
#
# App: - development (developer, local environment)
#      - acceptance (test environment in company)
#      - staging (test environment for client)
#      - master (final environment, production)
#
# 1. Get code from master branch
# 2. pip install
# 3. pytest -p no:warnings          -> fail
# 4. deploy to server
#
@pytest.fixture
def client():
    app.config['TESTING'] = True
    os.environ['DATABASE_URL'] = "sqlite:///:memory:"

    client = app.test_client()

    cleanup() # clean up db before test

    db.create_all()

    yield client

def cleanup():
    db.drop_all()

def test_index_not_logged_in(client):
    response = client.get('/')
    assert b'Enter your name' in response.data

def test_index_logged_in(client):
    client.post(
        '/login',
        data={"user-name": "bostjan", "user-email": "kolo@kolo.com", "user-password": "geslo"},
        follow_redirects=True
    )
    response = client.get('/')
    assert b'Enter your guess' in response.data

def test_users_show(client):
    name_one = faker.name()
    name_two = faker.name()
    name_three = faker.name()
    client.post(
        '/login',
        data={"user-name": name_one, "user-email": "hello@kolo.com", "user-password": "geslo"}
    )
    client.post(
        '/login',
        data={"user-name": name_two, "user-email": "marko@kolo.com", "user-password": "geslo"}
    )
    client.post(
        '/login',
        data={"user-name": name_three, "user-email": "matija@kolo.com", "user-password": "geslo"}
    )
    response = client.get('/users')
    assert bytes(name_one, 'utf-8') in response.data
    assert bytes(name_two, 'utf-8') in response.data
    assert bytes(name_three, 'utf-8') in response.data
