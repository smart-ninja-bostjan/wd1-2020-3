from cart import Cart

def test_cart_add_item():
    cart = Cart("bostjan")
    cart.add_to_cart(3, "burrata")
    cart.add_to_cart(5, "basil")

    items = cart.get_items()

    assert len(items) == 2

def test_cart_calculate_price():
    cart = Cart("bostjan")
    cart.add_to_cart(3, "burrata")
    cart.add_to_cart(5, "basil")

    price = cart.calculate_price()

    assert price == 8