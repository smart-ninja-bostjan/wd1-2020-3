list = [ 1, -5, -3, -2 ]

list.append( 5 )
list.append( 10 )
list.append( 15 )
list.append( 20 )

print( list )

# [ 1    -5    -3    -2    5    10    15    20 ]
#   0     1    2      3    4    5     6     7
#
# Array (list) indexes go from 0 - ( size of list - 1 )
#   0 -> 7 in our case

# Fetch single element by index
print( list[ 0 ] )
b = list[ 1 ]
print( b )

# First number is start, second number is end (upper limit)
# [ x: y ] -> If first number is not there, then x is 0
first_three = list[ :3 ]
print( first_three )

# Negative index?
last_element = list[ -1 ]
print( last_element )

# Sublist
sublist = list[ 2:4 ]
print( sublist )

list.insert( 3, 100000 )

print( list )

# 2D table (list)

"""
X O X
X O O
X X X
"""

tic_tac_toe = [
    [ "X", "O", "X" ], # 0
    [ "X", "O", "O" ], # 1
    [ "X", "X", "X" ]  # 2
#      0    1    2
]

top_first_element = tic_tac_toe[ 0 ][ 0 ]
print( top_first_element )