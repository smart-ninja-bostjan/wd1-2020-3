dictionary = {
    "height": 3.5,
    "size": 10,
    "value": 100,
    "text": "You shall not pass!"
}

height = dictionary[ "height" ]
print( height )

dictionary[ "weight" ] = "60kg"

print( dictionary )

dictionary[ "height" ] = "15m"

print( dictionary )

height = dictionary.get( "height" )

print( height )

"""
                    Dictionary vs. List
                    
List -> sequential order of stuff we add (0 - size - 1)
Dictionary -> order does not matter

Example of dictionary:

{
    "8290": "Sevnica",
    "1000": "Ljubljana"
}

Example of list: (NO - GO USE)

postNumbers = [ 8290, 1000 ]
postNames = [ "Sevnica", "Ljubljana" ]

"""