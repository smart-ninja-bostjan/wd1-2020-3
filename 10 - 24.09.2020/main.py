"""
emptyList = []
print( emptyList )

list = [ "banana", "apple", "prune" ]
print( list )

mixedList = [ True, "banana", 5, 5.3, None ]
print( mixedList )

numbersList = [ 1, 34, 12, 17, 87 ]
print( numbersList )

# Sort list from smallest to greatest
numbersList.sort()
print( numbersList )

# Print out each value in list
for number in numbersList:
    print( number )

# Add to list (to the end)
numbersList.append( 100 )

print( numbersList )

numbersList.sort( reverse=True )

print( numbersList )
"""

# []
# [ 2, 5, 1 ... ]
import json
import random
import datetime

secret = random.randint( 1, 30 )
attempts = 0

# Read score_list.txt
with open( "score_list.txt", "r" ) as score_file:
    # json.loads converts our text in file to a python data structure
    score_list = json.loads( score_file.read() )
    for score_dict in score_list:
        print( "Attempts made: {0}, Time of game: {1}".format( score_dict[ "attempts" ], score_dict[ "timestamp" ] ) )
        # print(str(score_dict["attempts"]) + " attempts, date: " + score_dict.get("date"))

while True:
    guess = int( input( "Guess the secret number (between 1 and 30): " ) )
    attempts += 1

    if guess == secret:
        print( "You've guessed it - congratulations! It's number " + str( secret ) )
        print( "Attempts needed: " + str( attempts ) )
        # Insert new attempts into file score_list.txt
        # BEFORE: score_list = []
        #                    = [ 5, 10, ... ]
        # AFTER with DICTIONARY:
        #         score_list = [
        #                           {
        #                                  "timestamp": "",
        #                                   "attempts": ...
        #                           }
        #                       ]
        score_data = {
            "timestamp": str( datetime.datetime.now() ),
            "attempts": attempts
        }
        score_list.append( score_data )

        with open( "score_list.txt", "w" ) as score_file:
            # json.dumps converts our data structure to text
            score_file.write( json.dumps( score_list ) )

        break
    elif guess > secret:
        print( "Your guess is not correct... try something smaller :)" )
    else:
        print( "Your guess is not correct... try something bigger :)" )

# GOAL: Store data of games TIMESTAMP + NUMBER OF ATTEMPTS
# 24.09.2020 18:03, 5
# 24.09:2020 18:05, 10
# 24.09:2020 18:10, 5

"""
attempt_one = {
    "timestamp": "24.09.2020 18:03",
    "attempts": 5
}

attempt_two = {
    "timestamp": "24.09.2020 18:05",
    "attempts": 10
}

attempt_three = {
    "timestamp": "24.09.2020 18:10",
    "attempts": 5
}

listOfAttempts = [ attempt_one, attempt_two, attempt_three ]
"""