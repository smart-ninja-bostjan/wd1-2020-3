class ShoppingCart():
    def __init__( self, first_name, last_name ):
        self.first_name = first_name
        self.last_name = last_name
        self.items = []

    def add_item( self, item ):
        self.items.append( item )

    def calculate_price( self ):
        price = 0
        for item in self.items:
            price = price + item.calculate_price()
        return price

    def print_cart_items( self ):
        for item in self.items:
            print( item )

class ShoppingCartItem():
    def __init__( self, name, price, quantity ):
        self.name = name
        self.price = price
        self.quantity = quantity

    def calculate_price( self ):
        return self.price * self.quantity

    def __str__( self ):
        return "{0} {1} {2}".format( self.name, self.price, self.quantity )

i1 = ShoppingCartItem( "Persil", 10, 2 )
i2 = ShoppingCartItem( "Lemons", 2, 3 )

cart = ShoppingCart( "Bostjan", "Cigan" )
cart.add_item( i1 )
cart.add_item( i2 )

print( cart.print_cart_items() )
print( cart.calculate_price() )

