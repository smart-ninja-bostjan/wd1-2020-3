# Variables - numbers
x = 7
y = 9
z = 5.098292

sumOfNumbers = x + y + z

print( sumOfNumbers )
print( x )
print( y )
print( z )

# Variables - strings (in quotes)
text = "This is going to be printed out!"
print( text )

anotherText = "Another text?"

# Concatenation of strings
joinedText = text + anotherText
print( joinedText )

# Another example ...
joinedTextWithSpace = text + " " + anotherText
print( joinedTextWithSpace )

# niz = "Abc"    niz * 5 = "AbcAbcAbcAbcAbc" -> some languages might offer that

# Boolean variable - Only two possible values - True or False
truth = True
nonTruth = False

# Data type None
blackHole = None

# Primitive data types
# - Number
#   - Integer -> whole number (1, -1, -10, 15 ...)
#   - Long -> whole number -> not in python
#   - Double -> decimal number
#   - Float -> decimal number (2.5, 3.14, ...) -> not in python
# - Strings
#   - "Hello world!"
# - Boolean
#   - True or False (1 or 0)
# - Nothing
#   - Stores nothing, just a variable that is pre-prepared

# [ 0 1 0 0 0 0 0 0 1 1 1 1 1 ]
#
# 1 1 0 0
# 0 0 0 0
# 0 0 0 0
# 0 0 0 0

# int a = 5;
# int b = 10;
# int c = a + b;
# b = "Smart Ninja!" -> NO GO IN JAVA e.g. strongly typed language

print( "Hello World!" )

name = input( "Please enter your name:" )

print( "Hello " + name + "!" )

a = input( "Enter number one: " )
b = input( "Enter number two: " )

sumOfNumbers = int( a ) + int( b )

# int ( enter data ) -> converts string to whole number

# a = "5"
# b = "3"
# c = a + b => "5" + "3" = "53"

print( "The sum is: " + str( sumOfNumbers ) )
print( sumOfNumbers )
# Can not join str + int = ???
# "5" + 6 = ? 56
# "5" + 6 = ? 11
# "5" + "a" = ? "5a"
# 5 + "a" = ? "5a" ??
# "5" + 6 = ? User tell me what is right!

# str ( enter data ) -> converts number to string

a = False - True
print( a )
# a = True (1)
# b = False (0)
# c = a - b -> 1 - 0 -> 1
# c = b - a -> 0 - 1 -> -1

# if POGOJ:
#   print( "Pogoj izpolnjen! ")

mood = input( "Enter mood (happy, nervous, anything else?): " )

if mood == "happy":
    print( "We are happy!" )
elif mood == "nervous":
    print( "We are sorry to hear that :( May we help you?" )
else:
    print( "Sorry but we do not know this state, so we can not help you." )