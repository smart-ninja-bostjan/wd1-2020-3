# def NAME_OF_FUNCTION( x, y, ... ):
#   insert your code here (one tab right)
#
# f( x, y ) = 3*x + 2*y
#    1  2
# def math( x, y ):
#   insert some code here

# f( x ) =
# VOID -> does not return anything!
def say_hello():
    print( "Hello!" )

# Call of function with no arguments:
# NAME_OF_FUNCTION()

# def NAME_OF_FUNCTION( name )
#                       -> arguments separated with comma
# VOID -> does not return anything!
def say_hello_with_name( name ):
    print( "Hello {0}!".format( name ) )

# RETURN STRING EXAMPLE
def say_hello_with_name_data( name ):
    data = "Hello {0}!".format( name )
    # Alternative if the code is short:
    # return "Hello {0}!".format(name)
    return data