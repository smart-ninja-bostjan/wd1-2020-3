from functions import say_hello, say_hello_with_name, say_hello_with_name_data
#    -> FILE NAME        -> WHICH FUNCTIONS WE ARE BRINGING IN FROM FILE

say_hello()
say_hello()
say_hello()
say_hello()
say_hello()

# say_hello_with_name()
# Prints out error because function needs argument name
say_hello_with_name( name="Bostjan" )

print( "---------------" )
name = say_hello_with_name_data( name="Bostjan" )
print( name )
# print( say_hello_with_name_data( name="Bostjan" ) )

# Write a function which sums two numbers!
# Write a function where you provide operation (*, -, /, ...) and two numbers and then the return
# of this operations

# When writing a function:
# 1. INPUT?
# 2. LOGIC OF FUNCTION
# 3. OUTPUT?

# 1. INPUT -> TWO NUMBERS, X, Y
# 2. ...
# 3. RETURN SUM -> ONE NUMBER
def sum( x, y ):
    return x + y

s = sum( 5, 10 )
print( s )

# 1. INPUT -> TWO NUMBERS, X, Y + OPERATION (*, -, /, +)
# 2. ...
# 3. OUTPUT RETURN NUMBER
def calculator( x, y, operation ):
    result = None

    if operation == "+":
        result = x + y
    elif operation == "*":
        result = x * y
    elif operation == "-":
        result = x - y
    elif operation == "/":
        result = x / y
    else:
        print( "Unsupported operation." )

    return result

r1 = calculator( 3, 5, "-" )
print( r1 )

r2 = calculator( 3, 5, "*" )
print( r2 )

r3 = calculator( 10, 20, "kolosej" )
print( r3 )

# Return statement in function can stop function execution before the final return
# statement
def dummy( x, y ):
    if x > 0:
        return x
    return 100

cifra = dummy( 200, 5 )
print( cifra )

# Optional arguments
def printNames( nameOne, nameTwo = None, nameThree = None ):
    print( nameOne )
    print( nameTwo )
    print( nameThree )

printNames( nameOne="Bostjan", nameTwo="Dejan" )

# Returning multiple values
def calculateMultiple( x, y ):
    # Returns sum, multiplication, division of two arguments
    r1 = x + y
    r2 = x / y
    r3 = x * y
    r4 = x - y

    return r1, r2, r3, r4

r1, r2, r3, r4 = calculateMultiple( 20, 10 )
print( r1 )
print( r2 )
print( r3 )
print( r4 )

_, _, _, substraction = calculateMultiple( 20, 30 )
print( substraction )

def hello():
    print( "3" )

for i in range( 0, 100 ):
    hello()

# RECURSION
# f( n ) = f ( n - 1 ) + f ( n - 2 )
# 1 1 ...
# 1 1 2 3 5 8 ...
