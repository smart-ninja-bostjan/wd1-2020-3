import os
from sqla_wrapper import SQLAlchemy

db = SQLAlchemy( os.getenv( "DATABASE_URL", "sqlite:///localhost.sqlite" ) )

class User( db.Model ):
    id = db.Column( db.Integer, primary_key= True )
    name = db.Column( db.String, unique = True )
    email = db.Column( db.String, unique = True )

# 1. name = Bostjan, email = bostjan.cigan@foo.com        PRIMARY-KEY = 1
#    -> OK, inserted into DB
# 2. name = Bostjan, email = bostjan.cigan@foo.com        PRIMARY-KEY = 2
#    -> FAILED, already exists