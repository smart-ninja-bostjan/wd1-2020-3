from flask import Flask, render_template, request, url_for, redirect, make_response
from models import User, db

# 1. RELATIONAL DATABASE (SQL - Structured Query Language)
#       -> MySQL, MariaDB, MSSQL (Microsoft), Oracle, PostgreSQL ...
# 2. NOSQL DATABASE
#       -> MongoDB, CouchDB, ...
# 3. GRAPH DATABASE
#       -> ArrangoDB, DGraph ...

# 1. RELATIONAL DB
# Tables -> columns, name       users -> email, name, id            one to many
#                               games -> date, tries, finished

# users entry:
#
# 1     bostjan@foo.com     bostjan
#
# games entry:
#                                                   user id -> user
# 1     19.10.2020          0           false       bostjan@foo.com (1)
# 2     19.10.2020          0           false       bostjan@foo.com (1)
#
# SQL -> Structured Query Language
#
# SELECT name, email FROM users WHERE email = "bostjan@foo.com"
#

app = Flask( __name__ )
db.create_all()

@app.route( "/" )
def index():
    email_address = request.cookies.get( "email" )

    # ORM -> Object Relationship Mapper
    if email_address:
        user = db.query( User ).filter_by( email=email_address ).first()
        # if not calling .first() we get a list -> user = user[ 0 ]
    else:
        user = None

    return render_template( "index.html", user=user )

@app.route( "/login", methods=[ "POST" ] )
def login():
    name = request.form.get( "user-name" )
    email = request.form.get( "user-email" )

    user = User( name=name, email=email )

    db.add( user )
    db.commit()

    response = make_response( redirect( url_for( 'index' ) ) )
    response.set_cookie( "email", email )
    return response

if __name__ == '__main__':
    app.run()