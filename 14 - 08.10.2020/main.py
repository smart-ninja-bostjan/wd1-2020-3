from flask import Flask, render_template

app = Flask( __name__ )

# xxx.xxx.xxx.xxx
# 127. 0 . 0 . 1    : xxx -> PORT
# 0 - 255
# IPv4 address

# IPv6 ...

# The string for the route is responsible for which page we are visiting
# / -> 127.0.0.1:5000/
# /yoda -> 127.0.0.1:5000/yoda
# ...
@app.route( "/" )
def index():
    return render_template( "index.html" )

# SPA -> Single Page Application
# Framework frontend -> React, Angular, Vue

# Backend -> /posts -> [ { ... } ]

if __name__ == '__main__':
    app.run() # app.run( port="5001" )
