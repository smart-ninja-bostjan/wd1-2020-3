import datetime
from flask import Flask, render_template, request, make_response

app = Flask( __name__ )

# HEROKU deployment
#
# What we need:
# 1. Procfile -> which server to run and which file to get
# 2. requirements.txt -> libraries to install
# 3. runtime.txt -> Which language version to run

# IP ADDRESS: 127.0.0.1 -> This one points to our local PC / MAC / Whatever
# PORT: 80 (IP:PORT) -> DEFAULT (HTTP - 80), 21 (FTP) ...

# JINJA
# {% ... %} - Control statement (if, block, for ...)
# {{ variable_name }} - Print out variable

# Template engines ->
#   - IF statements, FOR loops, blocks (chaining of templates)
#   - That's it! NO BUSINESS LOGIC!

# GET REQUESTS
# google.com/maps?location=Ljubljana&lat=70.593&lon=70.333
# -> the data in the address above:
#       location
#       lat
#       lon

# POST REQUESTS
# sending data to a server and processing it, connected to forms

@app.route( '/' )
def index():
    name = "Bostjan"
    cities = [ "Ljubljana", "Maribor", "Rome", "Vienna", "Stockholm" ]
    #cities = None # Triggers if statement (cities are not shown)
    year = datetime.datetime.now().year
    return render_template( "index.html", name=name, cities=cities, year=year )

@app.route( '/about', methods=[ "GET", "POST" ] )
def about():
    if request.method == "GET":
        user_name = request.cookies.get( "user_name" )
        user_email = request.cookies.get( "user_email" )
        return render_template( "about.html", name=user_name, email=user_email )
    elif request.method == "POST":
        contact_name = request.form.get( "contact-name" )
        contact_email = request.form.get( "contact-email" )
        contact_message = request.form.get( "contact-message" )

        response = make_response( render_template( "success.html" ) )
        response.set_cookie( "user_name", contact_name )
        response.set_cookie( "user_email", contact_email )

        return response

@app.route( '/opt-out', methods=[ "GET", "POST" ] )
def optout():
    if request.method == "GET":
        return render_template( "optout.html" )
    elif request.method == "POST":
        email = request.form.get( "email" )
        name = request.form.get( "name" )

        response = make_response( render_template( "optout-success.html" ) )

        if not email:
            #response.set_cookie( "user_name", "" )
            # With this the cookie still exists, only its value is empty
            response.set_cookie( "user_email", expires=0 )

        if not name:
            response.set_cookie( "user_name", expires=0 )

        return response

if __name__ == '__main__':
    app.run()