def render_template( file ):
    with open( 'templates/' + file, 'r' ) as template:
        template_content = template.read()
        return template_content

def get_all_variables( content ):
    opening = content.find( '{{' )
    closings = content.find( '}}' )

    year = content[ opening:closings+2 ]
    print( year )

content = render_template( "index.html" )
get_all_variables( content )