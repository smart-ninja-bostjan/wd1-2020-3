import random

secret = random.randint( 1, 30 )
guess = None

# While user does not guess the secret:   -> guess != secret
#   - Read number from user (guess)
#   - Check if guess is secret
#   - Print out relevant information

# WHILE LOOP
# while CONDITION:
#   execute statements (code)

# while 1 == 1:
#    print( "HELLO!" ) -> INFINITE LOOP

# WHILE LOOP example for game
"""
while guess != secret:
    guess = int( input( "Enter your guess: " ) )

    if guess == secret:
        print( "Success!" )
    else:
        print( "You did not guess!" )
"""

#   step   guess   secret   guess != secret
#    1     None     42        None != 42
#    2      2       42         2 != 42
#    3      3       42         3 != 42
#    4     42       42        42 != 42

# FOR LOOP
# for VARIABLE in range( NUMBER ):
#   execute code (loop)
# VARIABLE holds counter for how many times we executed the loop already
# VARIABLE starts at 0 and ends at NUMBER - 1

# BREAK
# Forcefully stop loop

"""
for counter in range( 5 ):
    guess = int( input( "Enter your guess: " ) )

    if guess == secret:
        print( "Success!" )
        break # This breaks our loop
    else:
        print( "You did not guess!" )
"""

# WHILE loop with break

# 10x => 10 * 200 => 2000 points

# 1. See comment bellow
# 2. If I enter END for input (guess) end program

while True: # 1 == 1, 0 == 0
    # guess = int( input( "Take your guess dude: " ) ) -> Initial solution

    # Exercise 2:
    guess = input( "Take your guess or write END to end game: " )
    # Guess can not be either a string or a number
    # We only do the conversion to INTEGER if variables guess is not END
    if guess == "END":
        break
    else:
        guess = int( guess )

    if guess == secret:                 # 100 points
        print( "You got it dude!" )
        break
    elif guess < secret:                # 100 points
        print( "Guess higher!" )
    else: # or elif guess > secret      # 100 points
        print( "Guess lower!" )

    # Exercise 1:
    # Ask user if he wants to end loop (y/n)

    """
    end = input( "Do you want to end the game? (y/n)? " )

    if end == "y":
        break
    """